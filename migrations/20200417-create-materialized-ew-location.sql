DROP MATERIALIZED VIEW IF EXISTS ew_location;
CREATE MATERIALIZED VIEW ew_location AS
    SELECT s.oid as oid_street, c.oid as oid_city, d.oid as oid_district, r.oid as oid_region,
    concat(s.item_name,' ',c.item_name) as full_item_name,
    s.item_name as item_name_street, c.item_name as item_name_city,
    d.item_name as item_name_district, r.item_name as item_name_region,
		s.item_code as item_code_street, c.item_code as item_code_city,
		d.item_code as item_code_district, r.item_code as item_code_region
    FROM "ew_street" s
    INNER JOIN "ew_city" c on c.oid = s.oid_city
    INNER JOIN "ew_district" d on d.oid = c.oid_district
    INNER JOIN "ew_region" r on d.oid_region = r.oid;


CREATE INDEX full_item_name_idx ON ew_location (full_item_name);
