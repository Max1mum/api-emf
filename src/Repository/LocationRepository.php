<?php

namespace App\Repository;

use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    public function findLocation($locationList)
    {
        $entityManager = $this->getEntityManager();
        $locationArray = [];
        foreach ($locationList as $location) {
            $locationArray[] = str_replace('__STRING__', $location, 'LOWER(l.fullItemName) LIKE LOWER(\'%__STRING__%\')');
        }
        $dql = 'SELECT l
            FROM App\Entity\Location l';
        if (!empty($locationList)) {
            $dql .= ' WHERE ';
            $dql .= implode(' AND ', $locationArray);
        }
        $dql .= 'ORDER BY l.fullItemName ASC';
        $query = $entityManager->createQuery($dql);

        return $query->getResult(AbstractQuery::HYDRATE_ARRAY);
    }
}
