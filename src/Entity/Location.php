<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 * @ORM\Table(name = "ew_location")
 */
class Location
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    private $oidStreet;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $oidCity;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $oidDistrict;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $oidRegion;
    /**
     * @ORM\Column(type="string", length=2000)
     *
     * @var string
     */
    private $fullItemName;
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $itemNameStreet;
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $itemNameCity;
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $itemNameDistrict;
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $itemNameRegion;
    /**
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    private $itemCodeStreet;
    /**
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    private $itemCodeCity;
    /**
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    private $itemCodeDistrict;
    /**
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    private $itemCodeRegion;

    /**
     * @return int
     */
    public function getOidStreet(): int
    {
        return $this->oidStreet;
    }

    /**
     * @param int $oidStreet
     */
    public function setOidStreet(int $oidStreet): void
    {
        $this->oidStreet = $oidStreet;
    }

    /**
     * @return int
     */
    public function getOidCity(): int
    {
        return $this->oidCity;
    }

    /**
     * @param int $oidCity
     */
    public function setOidCity(int $oidCity): void
    {
        $this->oidCity = $oidCity;
    }

    /**
     * @return int
     */
    public function getOidDistrict(): int
    {
        return $this->oidDistrict;
    }

    /**
     * @param int $oidDistrict
     */
    public function setOidDistrict(int $oidDistrict): void
    {
        $this->oidDistrict = $oidDistrict;
    }

    /**
     * @return int
     */
    public function getOidRegion(): int
    {
        return $this->oidRegion;
    }

    /**
     * @param int $oidRegion
     */
    public function setOidRegion(int $oidRegion): void
    {
        $this->oidRegion = $oidRegion;
    }

    /**
     * @return string
     */
    public function getFullItemName(): string
    {
        return $this->fullItemName;
    }

    /**
     * @param string $fullItemName
     */
    public function setFullItemName(string $fullItemName): void
    {
        $this->fullItemName = $fullItemName;
    }

    /**
     * @return string
     */
    public function getItemNameStreet(): string
    {
        return $this->itemNameStreet;
    }

    /**
     * @param string $itemNameStreet
     */
    public function setItemNameStreet(string $itemNameStreet): void
    {
        $this->itemNameStreet = $itemNameStreet;
    }

    /**
     * @return string
     */
    public function getItemNameCity(): string
    {
        return $this->itemNameCity;
    }

    /**
     * @param string $itemNameCity
     */
    public function setItemNameCity(string $itemNameCity): void
    {
        $this->itemNameCity = $itemNameCity;
    }

    /**
     * @return string
     */
    public function getItemNameDistrict(): string
    {
        return $this->itemNameDistrict;
    }

    /**
     * @param string $itemNameDistrict
     */
    public function setItemNameDistrict(string $itemNameDistrict): void
    {
        $this->itemNameDistrict = $itemNameDistrict;
    }

    /**
     * @return string
     */
    public function getItemNameRegion(): string
    {
        return $this->itemNameRegion;
    }

    /**
     * @param string $itemNameRegion
     */
    public function setItemNameRegion(string $itemNameRegion): void
    {
        $this->itemNameRegion = $itemNameRegion;
    }

    /**
     * @return string
     */
    public function getItemCodeStreet(): string
    {
        return $this->itemCodeStreet;
    }

    /**
     * @param string $itemCodeStreet
     */
    public function setItemCodeStreet(string $itemCodeStreet): void
    {
        $this->itemCodeStreet = $itemCodeStreet;
    }

    /**
     * @return string
     */
    public function getItemCodeCity(): string
    {
        return $this->itemCodeCity;
    }

    /**
     * @param string $itemCodeCity
     */
    public function setItemCodeCity(string $itemCodeCity): void
    {
        $this->itemCodeCity = $itemCodeCity;
    }

    /**
     * @return string
     */
    public function getItemCodeDistrict(): string
    {
        return $this->itemCodeDistrict;
    }

    /**
     * @param string $itemCodeDistrict
     */
    public function setItemCodeDistrict(string $itemCodeDistrict): void
    {
        $this->itemCodeDistrict = $itemCodeDistrict;
    }

    /**
     * @return string
     */
    public function getItemCodeRegion(): string
    {
        return $this->itemCodeRegion;
    }

    /**
     * @param string $itemCodeRegion
     */
    public function setItemCodeRegion(string $itemCodeRegion): void
    {
        $this->itemCodeRegion = $itemCodeRegion;
    }
}
