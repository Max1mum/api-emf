<?php

namespace App\Controller;

use App\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationController extends AbstractController
{
    public function getLocation($localityJson): JsonResponse
    {
        $locality = json_decode($localityJson, true);
        $locations = $this->getDoctrine()->getRepository(Location::class)->findLocation($locality);
        $subjects = [];
        foreach ($locations as $location) {
            foreach ($locality as $localityItem) {
                [$subjectId, $subjectsTemp] = $this->getSubject($location, $localityItem);
                $subjects[$subjectId] = $subjectsTemp;
            }
        }
        $subjects = array_values($subjects);
        $subjects = array_slice($subjects, 0, 9);   // returns "a", "b", and "c"

        return new JsonResponse($subjects);
    }

    /**
     * @param $location
     * @param $localityItem
     *
     * @return array
     */
    private function getSubject($location, $localityItem): array
    {
        if (false !== stripos($location['itemNameStreet'], $localityItem)) {
            $nameFormatted = $location['itemNameStreet'] . ' (obec ' . $location['itemNameCity'] . ')';
            $nameFormatted = str_ireplace($localityItem, '<b>' . $localityItem . '</b>', $nameFormatted);
            return [
                $location['fullItemName'],
                [
                    'codeType' => $location['itemCodeStreet'] . '-STREET',
                    'type' => 'STREET',
                    'nameFormatted' => $nameFormatted,
                    'parent' => [
                        'codeType' => $location['itemCodeCity'] . '-CITY',
                        'type' => 'CITY',
                        'nameFormatted' => null,
                        'parent' => null,
                        'name' => $location['itemNameCity'],
                        'code' => $location['itemCodeCity'],
                        'urlCode' => null,
                    ],
                    'name' => $location['itemNameStreet'],
                    'code' => $location['itemCodeStreet'],
                    'urlCode' => null,
                ]
            ];
        }
        if (false !== stripos($location['itemNameCity'], $localityItem)) {
            $nameFormatted = $location['itemNameCity'] . ' obec (okres ' . $location['itemNameDistrict'] . ')';
            $nameFormatted = str_ireplace($localityItem, '<b>' . $localityItem . '</b>', $nameFormatted);
            return
                [
                    $location['itemCodeCity'],
                    [
                        'codeType' => $location['itemCodeCity'] . '-CITY',
                        'type' => 'CITY',
                        'nameFormatted' => $nameFormatted,
                        'parent' => [
                            'codeType' => $location['itemCodeDistrict'] . '-DISTRICT',
                            'type' => 'DISTRICT',
                            'nameFormatted' => null,
                            'parent' => null,
                            'name' => $location['itemNameDistrict'],
                            'code' => $location['itemCodeDistrict'],
                            'urlCode' => null,
                        ],
                        'name' => $location['itemNameCity'],
                        'code' => $location['itemCodeCity'],
                        'urlCode' => null,
                    ]
                ];
        }

        return [];
    }
}
